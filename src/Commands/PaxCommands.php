<?php

namespace Drupal\pax\Commands;

use Drupal\Core\Config\FileStorage;
use Drush\Commands\DrushCommands;

class PaxCommands extends DrushCommands {

  /**
   * @hook pre-command config:edit
   */
  public function disablePaxOnConfigEdit() {
    if (property_exists(FileStorage::class, 'disableSharding')) {
      FileStorage::$disableSharding = TRUE;
    }
  }

}
