To get this module working, after installing it add the following two lines to
settings.php:

```
require_once 'modules/contrib/pax/src/ShardingFileStorage.php';
class_alias('Drupal\pax\ShardingFileStorage', 'Drupal\Core\Config\FileStorage');
```
